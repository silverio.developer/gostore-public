# GoStore - Nearby Stores, Offers & Events

Gostore is a search app that allows you to find local businesses and stores from a single or host of locations. As well as a user get exciting offers avail in their areas and different events within their area. Users can search stores and communicate in real time with owners Store and more. application built on Android Studio. Installation of server components is done quickly in a few simple steps.

## Features

- Login Phone with Verification Number
- Social Login with Google & Facebook
- Chat with Owners Stores
- Admob integration
- Push Notification
- Search Nearby Event & Stores

## Screenshot

![alt Text](screenshot/1.jpg "Screenshot")
![alt Text](screenshot/2.jpg "Screenshot")
![alt Text](screenshot/3.jpg "Screenshot")
![alt Text](screenshot/4.jpg "Screenshot")
![alt Text](screenshot/5.jpg "Screenshot")
![alt Text](screenshot/6.jpg "Screenshot")
![alt Text](screenshot/7.jpg "Screenshot")
![alt Text](screenshot/8.jpg "Screenshot")
![alt Text](screenshot/9.jpg "Screenshot")
![alt Text](screenshot/10.jpg "Screenshot")
![alt Text](screenshot/11.jpg "Screenshot")
![alt Text](screenshot/12.jpg "Screenshot")
![alt Text](screenshot/13.jpg "Screenshot")
![alt Text](screenshot/14.jpg "Screenshot")
![alt Text](screenshot/15.jpg "Screenshot")
![alt Text](screenshot/16.jpg "Screenshot")
![alt Text](screenshot/17.jpg "Screenshot")
![alt Text](screenshot/18.jpg "Screenshot")

## Support

[Rodrigo Silverio](mainto:silverio.developer@gmail.com)
